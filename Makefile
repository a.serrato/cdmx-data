.ONESHELL:
SHELL=/bin/bash
PYTHONBIN=/usr/bin/python3

VENV := env
DEPS := requirements.txt

process:
	@source $(VENV)/bin/activate
	python -i process.py
	@deactivate

dev:
	@source $(VENV)/bin/activate
	jupyter lab
	@deactivate

install-dep:
	@source $(VENV)/bin/activate
	pip install $(PKG)
	pip freeze > $(DEPS)
	@deactivate

freeze-deps:
	@source $(VENV)/bin/activate
	pip freeze > $(DEPS)
	@deactivate

init: create-venv upgrade-pip install-deps

install-deps:
	@source $(VENV)/bin/activate
	pip install -r $(DEPS)
	@deactivate

upgrade-pip:
	@source $(VENV)/bin/activate
	pip install --upgrade pip
	@deactivate

create-venv:
	$(PYTHONBIN) -mvenv $(VENV)

