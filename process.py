import unicodedata
import re
from urllib.parse import urlparse, parse_qs
import json
import time
import os.path

import requests
from bs4 import BeautifulSoup

BASE_URL = 'http://cdmxtravel.com'
labels_re = re.compile('LUGARES')
cat_re = re.compile('clasificación')
phone_re = re.compile('A tu disposición')
locat_re = re.compile('Cómo llegar')
descr_re = re.compile('En detalle')
places_re = re.compile('lugares/todas')
external_re = re.compile('Más información en')

destination_list_urls = [
    'http://cdmxtravel.com/es/destinos/centro-historico.html',
    'http://cdmxtravel.com/es/destinos/polanco-chapultepec.html',
    'http://cdmxtravel.com/es/destinos/alameda.html',
    'http://cdmxtravel.com/es/destinos/reforma-zona-rosa.html',
    'http://cdmxtravel.com/es/destinos/roma.html',
    'http://cdmxtravel.com/es/destinos/xochimilco.html',
    'http://cdmxtravel.com/es/destinos/coyoacan.html',
    'http://cdmxtravel.com/es/destinos/ciudad-universitaria.html',
    'http://cdmxtravel.com/es/destinos/condesa.html',
    'http://cdmxtravel.com/es/destinos/norte.html',
    'http://cdmxtravel.com/es/destinos/san-angel.html',
    'http://cdmxtravel.com/es/destinos/tlalpan.html',
    'http://cdmxtravel.com/es/destinos/villa-de-guadalupe.html',
    'http://cdmxtravel.com/es/destinos/santa-maria-magdalena-atlitic-los-dinamos.html',
    'http://cdmxtravel.com/es/destinos/world-trade-center.html',
    'http://cdmxtravel.com/es/destinos/cuajimalpa-desierto-de-los-leones.html',
    'http://cdmxtravel.com/es/destinos/iztacalco.html',
    'http://cdmxtravel.com/es/destinos/la-merced.html',
    'http://cdmxtravel.com/es/destinos/azcapotzalco.html',
    'http://cdmxtravel.com/es/destinos/culhuacan.html',
    'http://cdmxtravel.com/es/destinos/san-andres-mixquic.html',
    'http://cdmxtravel.com/es/destinos/san-pedro-atocpan.html',
    'http://cdmxtravel.com/es/destinos/santa-fe.html',
    'http://cdmxtravel.com/es/destinos/anzures.html',
    'http://cdmxtravel.com/es/destinos/escandon.html',
    'http://cdmxtravel.com/es/destinos/san-pedro-de-los-pinos.html',
    'http://cdmxtravel.com/es/destinos/santa-maria-la-ribera.html'
]

failed_urls = []

#####################
# Utility functions #
#####################

def download_url(url):
    print(f'Downloading: {url}')
    time.sleep(0.2)
    try:
        response = requests.get(url)
        page = BeautifulSoup(response.content, 'html.parser')
        return page
    except Exception as err:
        failed_urls.append(url)
        print(f'Failed to download url: {url}')
        raise err

def save_json(filename, obj):
    print(f'Saving: {filename}')
    try:
        with open(filename, 'w') as outfile:
            json.dump(obj, outfile, indent=4, ensure_ascii=False)
    except Exception as err:
        print(f'Failed to save file: {filename}')
        raise err

def open_json(filename):
    print(f'Opening: {filename}')
    try:
        with open(filename) as infile:
            return json.load(infile)
    except Exception as err:
        print(f'Failed to open file: {filename}')
        raise err

def normalize(txt):
    return unicodedata.normalize('NFKD', txt.strip())

###########################
# Destination information #
###########################

def download_destination(url):
    page = download_url(url)
    return page

def filter_destination(page):
    info = page.find(class_='ficha__principal____datos')
    return info

def destination_name(info):
    return normalize(info.find('h1').text)

def destination_description(info):
    return '\n\n'.join(filter(None, [
        normalize(par.text)
        for par in
        info.find(id='item-description').children
    ]))

def destination_places_url(info):
    anchor = info.find('a', {'href': places_re})
    if not anchor:
        return None
    return BASE_URL + anchor['href']

def parse_destination(info):
    data = {
        'name': destination_name(info),
        'description': destination_description(info),
        'places_url': destination_places_url(info)
    }
    return data

def process_destination(url):
    page = download_destination(url)
    info = filter_destination(page)
    data = parse_destination(info)
    data['url'] = url
    return data

###########################
# Destination places list #
###########################

def download_destination_places(url):
    page = download_url(url)
    return page

def last_page_destination_places(page):
    anchors = page.find(class_='pager').find_all('a')
    if not anchors:
        return 1
    last_page = parse_qs(urlparse(anchors[-1]['href'])[4])['page'][0]
    return int(last_page)

def download_all_destination_places(url, page):
    pages = [page]
    last_page = last_page_destination_places(page)
    for page_no in range(2, last_page+1):
        pages.append(download_destination_places(url + f'&page={page_no}'))
    return pages

def filter_destination_places(page):
    places_list = list(page.find(class_='resultado-listado__contenedor').children)
    return places_list

def parse_destination_places(places_list):
    place_references = []
    for place in places_list:
        anchor = place.find(class_='principal').find('a')
        place_references.append({
            'name': normalize(anchor.text),
            'url': anchor['href']
        })
    return place_references

def process_destination_places(url):
    initial_page = download_destination_places(url)
    pages = download_all_destination_places(url, initial_page)
    data = []
    for page in pages:
        places_list = filter_destination_places(page)
        data.extend(parse_destination_places(places_list))
    return data

#####################
# Place information #
#####################
def download_place(url):
    page = download_url(url)
    return page

def filter_place(page):
    info = page.find(class_='desarrollo-texto')
    return info

def place_name(info):
    return normalize(info.find('h1').text)

def place_labels(info):
    return [
        normalize(anchor.text)
        for anchor in
        info.find('span', string=labels_re).parent.parent.find_all('a')
    ]

def place_price(info):
    return normalize(info.find('span', string=labels_re).parent.parent.next_sibling.text)

def place_categories(info):
    try:
        return [
            normalize(cat)
            for cat in
            info.find('h4', string=cat_re).next_sibling.text.split(',')
        ]
    except:
        print('Missing categories')
        return None

def place_schedule(info):
    # From Monday to Sunday
    try:
        return [
            normalize(day.find_all('div')[-1].text)
            for day in
            info.find(class_='schedule').children
        ]
    except:
        print('Missing schedule')
        return None

def place_contact(info):
    try:
        return normalize(info.find('h4', string=phone_re).next_sibling.text)
    except:
        print('Missing contact number')
        return None

def place_location(info):
    try:
        anchor = info.find('h4', string=locat_re).next_sibling.find('a')
        return {
            'map_url': anchor['href'],
            'address': normalize(anchor.text)
        }
    except:
        print('Missing location')
        return None

def place_description(info):
    try:
        paragraphs = [
            normalize(par.text)
            for par in
            info.find('h4', string=descr_re).next_sibling.children
        ]
        return '\n\n'.join(paragraphs)
    except:
        print('Missing description')
        return None

def place_external_urls(info):
    try:
        return [
            anchor['href']
            for anchor in
            info.find('h4', string=external_re).next_sibling.children
        ]
    except:
        print('Missing external URLs')
        return None

def parse_place(info):
    data = {
        'name': place_name(info),
        'labels': place_labels(info),
        'price': place_price(info),
        'categories': place_categories(info),
        'schedule': place_schedule(info),
        'contact': place_contact(info),
        'location': place_location(info),
        'description': place_description(info),
        'external_urls': place_external_urls(info)
    }
    return data

def process_place(url):
    page = download_place(url)
    info = filter_place(page)
    data = parse_place(info)
    data['url'] = url
    return data

#####################
# Top-level process #
#####################
def process():
    for destination_idx, destination_url in enumerate(destination_list_urls):
        try:
            destination_data_filename = f'data/{destination_idx}_info.json'
            destination_data = {}
            if os.path.isfile(destination_data_filename):
                destination_data = open_json(destination_data_filename)
            else:
                destination_data = process_destination(destination_url)
                save_json(destination_data_filename, destination_data)
            if destination_data['places_url']:
                destination_places_filename = f'data/{destination_idx}_places.json'
                places = []
                if os.path.isfile(destination_places_filename):
                    places = open_json(destination_places_filename)
                else:
                    places = process_destination_places(destination_data['places_url'])
                    save_json(destination_places_filename, places)
                for place_idx, place in enumerate(places):
                    try:
                        place_filename = f'data/{destination_idx}_{place_idx}_info.json'
                        place_data = {}
                        if os.path.isfile(place_filename):
                            print(f'Skipping place {place_filename}')
                            continue
                        place_data = process_place(place['url'])
                        place_data['destination'] = destination_data['name']
                        save_json(place_filename, place_data)
                    except Exception as err:
                        print(err)
                        continue
        except Exception as err:
            print(err)
            continue
    print('Failed URLs:')
    print(failed_urls)
